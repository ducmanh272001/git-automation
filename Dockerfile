FROM openjdk:11
EXPOSE 8089
ADD target/hello-ok.jar hello-ok.jar
ENTRYPOINT ["java","-jar","/hello-ok.jar"]